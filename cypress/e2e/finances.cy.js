
describe('Transações', () => { 

    beforeEach(() => {   
        cy.visit("http://devfinance-agilizei.netlify.app") 
    });

    it('Cadastrar uma entrada', () => {  
        criarTransacao("Freela", 250) 

        cy.get("tbody tr td.description").should("have.text", "Freela") 
    });

    it('Cadastrar uma saída', () => { 
        criarTransacao("Cinema", -30) 

        cy.get("tbody tr td.description").should("have.text", "Cinema") 

    });

    it('Excluir transação', () => {
        criarTransacao("Freela", 100)  
        criarTransacao("Mesada", 13)

        cy.contains(".description", "Freela")    // caminho para chegar até a imagem do butão de excluir "X"
        .parent()     
        .find('img')   
        .click()       

        cy.get('tbody tr').should("have.length", 1) // a quantidade de linha de transação irá ser = 1
    });
});

function criarTransacao(descricao, valor){   // criação da função, juntamente com descrição e valores que irão ser atribuídos quando chamada
    cy.contains("Nova Transação").click()    // onde tiver no site o texto NOVA TRANSIÇÃO, clique!
    cy.get('#description').type(descricao)   // onde estiver o campo #description coloque o valor DESCRIÇÃO que foi dada la em cima
    cy.get('#amount').type(valor)            // onde estiver o campo #amount coloque o VALOR que foi dado la em cima
    cy.get('#date').type("2023-02-15")       // onde estiver o campo #DATE coloque o valor fixo da data 2023-02-15

    cy.contains('button', 'Salvar').click()  // onde conter o campo do tipo botão com nome de SALVAR, clique pra mim.
}
